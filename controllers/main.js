$(document).ready(function () {
    var callData = new CallData();
    var choseItemList = new ChoseItemList();

    renderHTML();
    
    function renderHTML() {
        callData
            .getData()
            .done(function (res) {
                console.log(res);

                var navPillsContent = "";
                var tabPanesContent = "";
                var classActive = "";

                res.navPills.forEach(function (item) {
                    classActive = item.tabName === "tabTopClothes" ? "active" : "";

                    navPillsContent += renderNavPills(item, classActive);
                    tabPanesContent += `
                    <div class="tab-pane container ${classActive}" id="${item.tabName}">
                        <div class="row">
                            ${renderTabPanes(res.tabPanes, item.type)}
                        </div>
                    </div>
                `;

                    $(".nav-pills").html(navPillsContent);
                    $(".tab-content").html(tabPanesContent);
                })
            })
            .fail(function (err) {
                console.log(err);
            })
    }

    function renderNavPills(item, classActive) {
        return `
            <li class="nav-item">
                <a class="nav-link btn-default ${classActive}" data-toggle="pill" href="#${item.tabName}">
                    ${item.showName}
                </a>
            </li>
        `;
    }

    function creatAtempArr(tabPanesArr, navPillsType) {
        var tempArr = [];

        tabPanesArr.forEach(function(item) {
            if (item.type === navPillsType) {
                tempArr.push(item);
            }
        })

        return tempArr;
    }

    function renderTabPanes(tabPanesArr, navPillsType) {
        var content = "";
        var tempArr = creatAtempArr(tabPanesArr, navPillsType);

        tempArr.forEach(function(item) {
            content += `
                <div class="col-md-3">
                    <div class="card text-center">
                        <img src="${item.imgSrc_jpg}" />
                        <h4><b>${item.name}</b></h4>
                        <button data-id="${item.id}" data-type="${item.type}" data-name="${item.name}" data-desc="${item.desc}" data-imgsrcjpg="${item.imgSrc_jpg}" data-imgsrcpng="${item.imgSrc_png}" class="changeStyle">Try it</button>
                    </div>
                </div>
            `;
        })

        return content;
    }

    function findIndex(choseItem) {
        var index = -1;

        if (choseItemList.list.length > 0) {
            choseItemList.list.forEach(function (item, i) {
                if (item.type === choseItem.type) {
                    return index = i;
                }
            })
        }

        return index;
    }

    $("body").delegate(".changeStyle", "click", function() {
        var id = $(this).data("id");
        var type = $(this).data("type");
        var name = $(this).data("name");
        var desc = $(this).data("desc");
        var imgSrc_jpg = $(this).data("imgsrcjpg");
        var imgSrc_png = $(this).data("imgsrcpng");

        var choseItem = new ChoseItem(id, type, name, desc, imgSrc_jpg, imgSrc_png);
        
        var index = findIndex(choseItem);
        
        if (index === -1) {
            choseItemList.addItem(choseItem);
        } else {
            choseItemList.list[index] = choseItem;
        }

        showItem(choseItemList.list);
    })

    function showItem(itemList) {
        if (itemList.length > 0) {
            itemList.forEach(function(item) {
                if (item.type === "topclothes") {
                    renderBikinitop(item.imgSrc_png);
                }
                if (item.type === "botclothes") {
                    renderBikinibottom(item.imgSrc_png);
                }
                if (item.type === "shoes") {
                    renderFeet(item.imgSrc_png);
                }
                if (item.type === "handbags") {
                    renderHandbag(item.imgSrc_png);
                }
                if (item.type === "necklaces") {
                    renderNecklace(item.imgSrc_png);
                }
                if (item.type === "hairstyle") {
                    renderHairstyle(item.imgSrc_png);
                }
                if (item.type === "background") {
                    renderBackground(item.imgSrc_png);
                }
            })
        }
    }

    function renderBikinitop(img) {
        $(".bikinitop").css({
            width: "500px",
            height: "500px",
            background: `url(${img})`,
            position: "absolute",
            top: "-9%",
            left: "-5%",
            zIndex: "3",
            transform: "scale(0.5)"
        })
    }

    function renderBikinibottom(img) {
        $(".bikinibottom").css({
            width: "500px",
            height: "1000px",
            background: `url(${img})`,
            position: "absolute",
            top: "-30%",
            left: "-5%",
            zIndex: "2",
            transform: "scale(0.5)"
        })
    }

    function renderFeet(img) {
        $(".feet").css({
            width: "500px",
            height: "1000px",
            background: `url(${img})`,
            position: "absolute",
            bottom: "-37%",
            right: "-3.5%",
            transform: "scale(0.5)",
            zIndex: "1"
        })
    }

    function renderHandbag(img) {
        $(".handbag").css({
            width: "500px",
            height: "1000px",
            background: `url(${img})`,
            position: "absolute",
            bottom: "-40%",
            right: "-3.5%",
            transform: "scale(0.5)",
            zIndex: "4"
        })
    }

    function renderNecklace(img) {
        $(".necklace").css({
            width: "500px",
            height: "1000px",
            background: `url(${img})`,
            position: "absolute",
            bottom: "-40%",
            right: "-3.5%",
            transform: "scale(0.5)",
            zIndex: "4"
        })
    }

    function renderHairstyle(img) {
        $(".hairstyle").css({
            width: "1000px",
            height: "1000px",
            background: `url(${img})`,
            position: "absolute",
            top: "-75%",
            right: "-57%",
            transform: "scale(0.15)",
            zIndex: "4"
        })
    }

    function renderBackground(img) {
        $(".background").css({
            backgroundImage: `url(${img})`
        })
    }

})